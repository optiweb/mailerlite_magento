<?php
/**
 * @category   Optiweb
 * @package    Optiweb_MailerLite
 * @author     ales.cankar@optiweb.com
 * @website    http://www.optiweb.com
 * @license    https://opensource.org/licenses/MIT  MIT License
 * @date	    5.1.2018
 */

class Optiweb_MailerLite_Model_Observer extends Mage_Core_Model_Abstract
{
    const STATUS_SUBSCRIBED = 1;
    const STATUS_NOT_ACTIVE = 2;
    const STATUS_UNSUBSCRIBED = 3;
    const STATUS_UNCONFIRMED = 4;
    const DEBUG_FILE = 'mailerLite.log';
    
    private $_apiKey;
    private $_listId;
    private $_debug;
    
    public function __construct()
    {
        $this->_apiKey = Mage::getStoreConfig('optiweb_ml/mailerlite/mailerlite_apikey');
        $this->_listId = Mage::getStoreConfig('optiweb_ml/mailerlite/mailerlite_listid');
        $this->_debug = Mage::getStoreConfig('optiweb_ml/mailerlite/enable_debug');
    }
    
    public function subscribe(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();
        $subscriber = $event->getDataObject();
        $data = $subscriber->getData();
        $statusChange = $subscriber->getIsStatusChanged();
        if (Mage::getStoreConfig('optiweb_ml/mailerlite/enabled') == 1) {
            if ($data['subscriber_status'] == "1" && $statusChange == true) {
                $email = $data ['subscriber_email'];
                $this->_addNew($email);
            } else {
                $email = $data['subscriber_email'];
                if (isset($data['subscriber_status']) && $data['subscriber_status'] == self::STATUS_UNSUBSCRIBED) {
                    $this->_unsubscribe($email);
                }
            }
        }
        return true;
    }
    
    /**
     * Subscribe a user to a MailerLite list
     * @param $data
     * @return bool
     */
    private function _addNew($email)
    {
        try {
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => "https://api.mailerlite.com/api/v2/groups/{$this->_listId}/subscribers",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode([
                    'email' => $email,
                    'type' => 'subscribed'
                ]),
                CURLOPT_HTTPHEADER => [
                    "content-type: application/json",
                    "x-mailerlite-apikey: {$this->_apiKey}",
                ],
            ]);
            $return = curl_exec($curl);
            $response = json_decode($return, true);
            $err = curl_error($curl);
            curl_close($curl);
            if ($this->_debug) {
                if ($err || isset($response['error'])) {
                    Mage::log('Optiweb_Mailerlite cUrl error:');
                    if ($err) {
                        Mage::log("Optiweb_Mailerlite cUrl error: {$err}", null, self::DEBUG_FILE);
                    } else {
                        Mage::log("Optiweb_Mailerlite cUrl error: {$response['error']['message']}", null, self::DEBUG_FILE);
                    }
                    Mage::getSingleton('core/session')->getMessages(true);
                    Mage::getSingleton('core/session')->addError($response['error']['message']);
                    return false;
                } else {
                    Mage::log('Optiweb_Mailerlite: Subscribed ' . $email, null, self::DEBUG_FILE);
                }
            }
            return true;
            
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, self::DEBUG_FILE, true);
        }
        return true;
    }
    
    /**
     * Unsubscribe a user from a MailerLite list
     * @param $email
     * @return bool
     */
    private function _unsubscribe($email)
    {
        try {
            $apikey = Mage::getStoreConfig('optiweb_ml/mailerlite/mailerlite_apikey');
            $listId = Mage::getStoreConfig('optiweb_ml/mailerlite/mailerlite_listid');
            
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => "https://api.mailerlite.com/api/v2/groups/{$listId}/subscribers",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode([
                    'email' => $email,
                    'type' => 'unsubscribed'
                ]),
                CURLOPT_HTTPHEADER => [
                    "content-type: application/json",
                    "x-mailerlite-apikey: {$apikey}",
                ],
            ]);
            $return = curl_exec($curl);
            $response = json_decode($return, true);
            $err = curl_error($curl);
            curl_close($curl);
            if ($this->_debug) {
                if ($err || isset($response['error'])) {
                    if ($err) {
                        Mage::log("Optiweb_Mailerlite cUrl error: " . $err, null, self::DEBUG_FILE);
                    } else {
                        Mage::log("Optiweb_Mailerlite cUrl error: " . $response['error']['message'], null, self::DEBUG_FILE);
                    }
                    Mage::getSingleton('core/session')->getMessages(true);
                    Mage::getSingleton('core/session')->addError($response['error']['message']);
                    return false;
                } else {
                    Mage::log('Optiweb_Mailerlite: Unubscribed ' . $email, null, self::DEBUG_FILE);
                }
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, self::DEBUG_FILE, true);
        }
        return true;
    }
    
}
#Mailerlite integration for Magento
This extension will connect your Magento 1 store with Mailerlite mailing service.

___

##Features:
- add subscribers via:
	- any newsletter form
	- user dashboard
	- admin panel
	- checkout
- unsubscribe
- choose different groups per store view
- enable debugging

##Compatibility:
Tested in Magento 1.9.2.3. It should work with all Magento 1 installations

##Known issues:
- once a user re-subscribes to your newsletter, they will recieve an error message 'Subscriber type is unsubscribed'. In Mailerlite they will not resubscribe, but in Magento they will. This is a technical limitation on MailerLite.

##Installation:
1. download a zip and upload contents to the root folder of your Magento 1 installation
2. log out of the admin panel and log back in
3. go to System > Configuration > Optiweb > MailerLite and enable it
4. login to Mailerlite and copy the API key to configuration from here <https://app.mailerlite.com/integrations/api/>
5. copy list ID to configuration. You can get it by opening it and copying it from the URL ie. https://app.mailerlite.com/subscribers/view/__XXXXXXX__

##Licence:
MIT. (see LICENSE.txt)
